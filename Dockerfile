FROM debian:stable-slim
MAINTAINER https://gitlab.com/gregoryking/docker-linux-gcc

RUN apt update; \
    apt install -y gnupg build-essential git-core ca-certificates libltdl-dev \
    python python-dev python-pip python3 python3-dev python3-pip ninja-build \
    gcc-multilib pkg-config libffi-dev cmake zip m4 manpages-pl gettext \
    autoconf autotools-dev automake autogen libtool 

RUN pip install -U pip setuptools wheel && \
    pip3 install -U pip setuptools wheel pipenv cpp-coveralls

# Print out the version of GCC installed during the build process
RUN VERSION=$(dpkg -s gcc| grep '^Version:' | sed -rn 's/Version: .*?:(.*?)-.*/\1/p');\
    echo "VERSION=${VERSION}"

# Clean up cache
RUN rm -rf /var/cache/apk/*
